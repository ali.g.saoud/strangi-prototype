# StrAnGI AGI

This project aims to use GPT agents to test out cognitive science theories regarding Strang Loop cognition.

## How it works

This project uses [OpenAI API](https://openai.com/api/) and [LangChain](https://github.com/hwchase17/langchain), uses agents and custom reward functions for GPT.

The current iteration of the system: ![system](./sis.png)
